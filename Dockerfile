FROM node:alpine

RUN apk update && apk upgrade

WORKDIR '/group2'

COPY ./voting-app/package*.json .
RUN npm install

COPY ./voting-app .

CMD ["npm", "run", "start"]
