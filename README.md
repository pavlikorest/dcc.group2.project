1) Set up Gitlab CI for current project (create .gitlab-ci.yml, install Gitlab runner with docker and other necessary tools)

2) .gitlab-ci.yml -> stage_1: Create new EC2 instance on AWS using Terraform (main.tf, put AWS credentials to terraform.tfvarsterraform.tfvars [Check out https://wilsonmar.github.io/terraform/ -> Credentials in tfvars]) 

3) .gitlab-ci.yaml -> stage_2: Create new EKS cluster on AWS https://github.com/k-mitevski/terraform-k8s/blob/master/01_terraform_eks/main.tf (use instead of this file my working one, updated according to Terraform 0.15.1v)

4) .gitlab-ci.yaml -> stage_3: build Docker image of Voting NodeJS app from Dockerfile, push it to Docker registry

5) .gitlab-ci.yaml -> stage_4: Think about: either via Terraform  https://github.com/cloudposse/terraform-aws-ecs-web-app / https://github.com/nickbatts/tf-docker-nodejs-app/blob/master/terraform-docker-nodejs-server.tf OR 
via aws ecs deploy and run NodeJS app from Docker image to K8S cluster 
- aws ecs update-service --cluster to_be_provided --service redis-flask --force-new-deployment

TO DO TASKS:
- Terraform - make Public IP4 address and assign SSH key to created instance
- ReplicaSet + autoscale
- HTTPS certificate - 3 month renewal cycle
